package mini.project.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import mini.project.BirdVsBee;
import mini.project.GameMain;
import mini.project.GameManager;
import mini.project.MainMenu;
import mini.project.MyGdxGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 1280 ;
		config.height = 720;
		new LwjglApplication(new GameMain() , config);
		//new LwjglApplication(new BirdVsBee()),config);
		//new LwjglApplication(new MainMenu(), config);
	}
}
