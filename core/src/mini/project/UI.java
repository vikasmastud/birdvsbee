package mini.project;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

public class UI {
        private Stage stage;
        Label scoreLabel , startMessage , reStartMessage;
        int score ;

        public UI()
        {

            stage = new Stage(new ScreenViewport());
            createLabel();
           // stage.addActor(startMessage);
            //stage.addActor(reStartMessage);
            stage.addActor(scoreLabel);
        }
        void setScore(int score){
            scoreLabel.setText(String.valueOf(score));
        }

        void createLabel() {
            FreeTypeFontGenerator generator = new FreeTypeFontGenerator(
                    Gdx.files.internal("04b_19.ttf"));
            FreeTypeFontGenerator.FreeTypeFontParameter parameter =
                    new FreeTypeFontGenerator.FreeTypeFontParameter();
            parameter.size = 50;
            BitmapFont font = generator.generateFont(parameter);
            scoreLabel = new Label(String.valueOf(score),
                    new Label.LabelStyle(font, Color.BLUE));
            scoreLabel.setPosition(30,
                    650);
        }

        int getScore(){
            return score;
        }
        void createRestartMessage() {
            FreeTypeFontGenerator generator = new FreeTypeFontGenerator(
                    Gdx.files.internal("04b_19.ttf"));
            FreeTypeFontGenerator.FreeTypeFontParameter parameter =
                    new FreeTypeFontGenerator.FreeTypeFontParameter();
            parameter.size = 100;
            BitmapFont font = generator.generateFont(parameter);
            scoreLabel = new Label(String.valueOf("Tap to Start"),
                    new Label.LabelStyle(font, Color.WHITE));
            scoreLabel.setPosition(Gdx.graphics.getWidth()/3+50 , Gdx.graphics.getHeight()/2);
        }
        void createStartMessage() {
            FreeTypeFontGenerator generator = new FreeTypeFontGenerator(
                    Gdx.files.internal("04b_19.ttf"));
            FreeTypeFontGenerator.FreeTypeFontParameter parameter =
                    new FreeTypeFontGenerator.FreeTypeFontParameter();
            parameter.size = 100;
            BitmapFont font = generator.generateFont(parameter);
            startMessage = new Label(String.valueOf("You Loss!! Tap to Re-start game"),
                    new Label.LabelStyle(font, Color.WHITE));
            startMessage.setPosition(Gdx.graphics.getWidth() / 3 + 50 , Gdx.graphics.getHeight()/2);
        }
        public Stage getStage(){
            return stage;
        }


}




