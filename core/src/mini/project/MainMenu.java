package mini.project;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Texture;

public class MainMenu implements Screen {

    private GameManager gm;
    private Texture bg;
    GameMain game;
    public MainMenu(GameMain game){
        this.game = game;
        bg = new Texture("baground/ice4.png");
        gm = new GameManager(game);
    }
    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {

        game.getBatch().begin();
        game.getBatch().draw(bg , 0 , 0 , Gdx.graphics.getWidth() , Gdx.graphics.getHeight() );
        game.getBatch().end();

        gm.getStage().draw();

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        gm.getStage().dispose();
        bg.dispose();
    }



}
