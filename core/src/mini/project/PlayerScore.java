package mini.project;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

public class PlayerScore {
        private Stage stage;
        Label scoreLabel , startMessage , reStartMessage;
        int score ;

        public PlayerScore()
        {
            stage = new Stage(new ScreenViewport());
            createLabel();
            // stage.addActor(startMessage);
            //stage.addActor(reStartMessage);
            stage.addActor(scoreLabel);
        }
        void setScore(String score){
            scoreLabel.setText(String.valueOf(score));
        }

        void createLabel() {
            FreeTypeFontGenerator generator = new FreeTypeFontGenerator(
                    Gdx.files.internal("04b_19.ttf"));
            FreeTypeFontGenerator.FreeTypeFontParameter parameter =
                    new FreeTypeFontGenerator.FreeTypeFontParameter();
            parameter.size = 50;
            BitmapFont font = generator.generateFont(parameter);
            scoreLabel = new Label(String.valueOf(" Your Score "+score),
                    new Label.LabelStyle(font, Color.BLUE));
            scoreLabel.setPosition(Gdx.graphics.getWidth()/3,
                    500);
        }

        int getScore(){
            return score;
        }

        public Stage getStage(){
            return stage;
        }

}
