package mini.project;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;


import org.omg.PortableInterceptor.Interceptor;

import java.util.Random;

public class BirdVsBee extends ApplicationAdapter {
    SpriteBatch batch;

    String birdColor;
    Sound winSound , damageSound , jumpSound;
    Texture img , bird;
    TextureAtlas birdAtlas;
    TextureAtlas beesAtlas;
    BitmapFont scoreF , startM;
    Animation<TextureRegion> animate;
    Animation<TextureRegion> beeFlap1;
    Animation<TextureRegion> beeFlap2;
    Animation<TextureRegion> beeFlap3;
    int life =0;
    float sw , sh , bw , bh , bx , by;
    float velocity = 0.0f , gravity = 0.1f;
    int state = 0;
    int bees = 3;
    boolean damageB;
    float[] beesx = new float[bees];
    ShapeRenderer sr ;
    Rectangle c_bird , c_bee1[] , c_bee2[] , c_bee3[];
    float[][] beesy = new float[bees][bees];
    Random r1;
    int score=210;
    float elapsedTime = 0.0f;
    boolean winB;
    Label scoreLabel , startMessage , reStartMessage;
    UI ui;
    StartMesseg sm;
    ReStartMessage rsm;
    @Override
    public void create () {
        batch = new SpriteBatch();
        img = new Texture("baground/ice4.png");
        sr = new ShapeRenderer();
        rsm = new ReStartMessage();
        damageB = false;
        ui = new UI();
        sm = new StartMesseg();
//		jumpSound = Gdx.audio.newSound(Gdx.files.internal("jump.ogg"));
//		damageSound = Gdx.audio.newSound(Gdx.files.internal("damage.ogg"));
        winSound = Gdx.audio.newSound(Gdx.files.internal("win.ogg"));

        scoreF = new BitmapFont();
        scoreF.setColor(Color.ROYAL);
        scoreF.getData().setScale(2);


        startM = new BitmapFont();
        startM.setColor(Color.WHITE);
        startM.getData().setScale(2);

        sw = Gdx.graphics.getWidth();
        sh =  Gdx.graphics.getHeight();
        bw =  Gdx.graphics.getWidth()/13;
        bh =  Gdx.graphics.getHeight()/11;
        bx =  Gdx.graphics.getHeight() / 4 ;
        by =  Gdx.graphics.getHeight() / 2;

//	    bee1 = new Texture("1.png");
//	    bee2 = new Texture("1.png");
//	    bee3 = new Texture("1.png");

        birdAtlas =  new TextureAtlas("red/red.atlas");
        animate = new Animation<TextureRegion>(1f/7f , birdAtlas.getRegions() );

        beesAtlas = new TextureAtlas("bees/bees.atlas");
        beeFlap1 = new Animation<TextureRegion>(1/7f , beesAtlas.getRegions());

        beesAtlas = new TextureAtlas("bees/bees.atlas");
        beeFlap2 = new Animation<TextureRegion>(1/7f , beesAtlas.getRegions());

        beesAtlas = new TextureAtlas("bees/bees.atlas");
        beeFlap3 = new Animation<TextureRegion>(1/7f , beesAtlas.getRegions());


        c_bird = new Rectangle();
        c_bee1 = new Rectangle[bees];
        c_bee2 = new Rectangle[bees];
        c_bee3 = new Rectangle[bees];
        for(int i = 0 ; i < bees ; i++){
            beesx[i] = sw + i * (sw / 2) ;
            r1 = new Random();
            beesy[0][i] =  r1.nextInt((200)+1 )+200 +r1.nextFloat();
            if(score>100)beesy[1][i] =  r1.nextInt((150)+1 )+100 +r1.nextFloat();
            if(score>200)beesy[2][i] =  r1.nextInt((100)+1 )+200 +r1.nextFloat();

            c_bee1[i] = new Rectangle();
            c_bee2[i] = new Rectangle();
            c_bee3[i] = new Rectangle();
        }
    }

    @Override
    public void render () {



        batch.begin();
        batch.draw(img , 0 ,0  , sw ,sh);

//		batch.draw(bird , bx , by , bw , bh);

        elapsedTime += Gdx.graphics.getDeltaTime();
        TextureRegion currentBirdFrame = animate.getKeyFrame(elapsedTime , true);
        batch.draw( currentBirdFrame , bx , by , bw , bh);

        TextureRegion currentBeeFrame1 = beeFlap1.getKeyFrame(elapsedTime , true);
        TextureRegion currentBeeFrame2 = beeFlap2.getKeyFrame(elapsedTime , true);

        TextureRegion currentBeeFrame3  = beeFlap3.getKeyFrame(elapsedTime , true);

        if(state == 1) {

//			for(int i = 0 ; i < bees ; i++){
//				System.out.println(beesx[i] + " "+beesy[0][i] +" "+beesy[1][i]+" "+beesy[2][i]);
//			}
            if (Gdx.input.isTouched()) {
                velocity = -3;
            }
            for(int i = 0 ; i < bees ; i++){
                if(beesx[i] < 0){
                    score++;
                    ui.setScore(score);
                    if (score %50 == 0) winB = true;
                    beesx[i] = bees *  sw / 2 ;
                    beesy[0][i] =  r1.nextInt(650) +r1.nextFloat();
                    if(score>100 )beesy[1][i] =  r1.nextInt(650) +r1.nextFloat();
                    if(score>200) beesy[2][i] = r1.nextInt(650) + r1.nextFloat();
                }
                beesx[i] -= 6;
                batch.draw(currentBeeFrame1 , beesx[i] , beesy[0][i] , bw , bh);
                if(score>100)	batch.draw(currentBeeFrame2 , beesx[i] , beesy[1][i] , bw , bh);
                if(score>200)	batch.draw(currentBeeFrame3 ,beesx[i] , beesy[2][i] , bw , bh);

            }
            //sr.begin(ShapeRenderer.ShapeType.Filled);
            if( score % 50 == 0 && winB) {
                winSound.play();
                winB = false;
            }
//            startM.draw(batch , score+"" , sw - 100 , 50);
            ui.getStage().draw();
            ui.getStage().act();
            //sr.end();
            if(by > 650){
                by = 650;

            }
            if (by < 0) {
                by = Gdx.graphics.getHeight() / 3;
                velocity = 0;
                state = 2;
            }
            velocity = velocity + gravity;
            by = by - velocity;
        }else if(state ==0){
            Preferences pref = Gdx.app.getPreferences("Data");
//            startM.draw(batch, "High Score is "+pref.getInteger("Score"), sw/3 + 50 , sh /2 + 50);
//            startM.draw(batch,"Tap to Start Game", sw/3+50 , sh/2);
            sm.getStage().draw();
            sm.getStage().act();
            if(Gdx.input.justTouched()){
                state = 1;
            }
        }else if(state == 2){

//            startM.draw(batch,"You loss!! Tap to Re-Start Game" , sw/3+50, sh/2);
            Preferences pref = Gdx.app.getPreferences("Data");
//            startM.draw(batch, "High Score is "+pref.getInteger("Score"), sw/3 + 50 , sh /2 + 50);
            rsm.getStage().draw();
            rsm.getStage().act();
            int highSore =  pref.getInteger("Score");
            if(score > highSore){
                pref.putInteger("Score" , score);
                pref.flush();
            }
            if(Gdx.input.justTouched()){
                //score = 0;
                for(int i = 0 ; i < bees ; i++){
                    beesx[i] = sw + i * (sw / 2) ;
                    r1 = new Random();
                    beesy[0][i] =  r1.nextInt((200)+1 )+200 +r1.nextFloat();
                    if(score>100)	beesy[1][i] =  r1.nextInt((150)+1 )+100 +r1.nextFloat();
                    if(score>200)	beesy[2][i] =  r1.nextInt((100)+1 )+200 +r1.nextFloat();

                    c_bee1[i] = new Rectangle();
                    if(score>100)	c_bee2[i] = new Rectangle();
                    if(score>200)	c_bee3[i] = new Rectangle();
                }
                state = 1;
                score = 0;
                ui.setScore(0);
            }
        }
        sr.begin(ShapeRenderer.ShapeType.Filled);
        c_bird.set(bw + bw/2 + bw -40   , by +20, bw/2 , bh/2);
        for(int i = 0 ; i < bees; i++){
//				sr.setColor(Color.BLUE);
//				sr.rect(bw + bw/2 + bw -40   , by +20, bw/2 , bh/2);
//				sr.rect(beesx[i] + bw/2 - 35, beesy[0][i] + bh /2 -20 , bw/2 , bh/2);
//				sr.rect(beesx[i] +bw/2 -35, beesy[1][i] + bh/2  -20, bw/2 , bh/2);
//				sr.rect(beesx[i] +bw/2 -35, beesy[2][i] + bh/2 -20 , bw/2 , bh/2);
				//sr.circle(bx + bw/2 , by + bh/2 , bw/4);

            c_bee1[i].set(beesx[i] + bw/2 - 35, beesy[0][i] + bh /2 -20 , bw/2 , bh/2);
            if(score>100)c_bee2[i].set(beesx[i] +bw/2 -35, beesy[1][i] + bh/2  -20, bw/2 , bh/2);
            if(score>200)c_bee3[i].set(beesx[i] +bw/2 -35, beesy[2][i] + bh/2 -20 , bw/2 , bh/2);

            if(score<100){
                if(Intersector.overlaps(c_bird , c_bee1[i])){
                    state = 2;
                }
            }
            if(score>100){
                if(Intersector.overlaps(c_bird , c_bee1[i]) || Intersector.overlaps(c_bird , c_bee2[i])){
                    state = 2;
                }
            }

            if(score>200) {
                if (Intersector.overlaps(c_bird, c_bee1[i]) || Intersector.overlaps(c_bird, c_bee2[i]) || Intersector.overlaps(c_bird, c_bee3[i])) {
                    state = 2;
                }
            }
//			float xD = c_bird.x - c_bee1[i].x;      // delta x
//			float yD = c_bird.y - c_bee1[i].y;      // delta y
//			float sqDist = xD * xD + yD * yD;  // square distance
//			boolean collision = sqDist <= (c_bird.radius+c_bee1[i].radius) * (c_bird.radius+c_bee1[i].radius);
//
//			if(collision){
//				state=2;
//			}
        }
        	sr.end();
        batch.end();
    }

    @Override
    public void dispose () {
        batch.dispose();
        img.dispose();
    }
}
