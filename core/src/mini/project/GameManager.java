package mini.project;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;

public class GameManager {
    private Stage stage;

    //my
    ImageButton red , blue , grey ,yellow , grumppy , blueHat;
    Label scoreLabel;
    String player;
    Table table;
    Texture icebg;
    GameMain game;
    Viewport gameViewport;
    int score;
    public GameManager(GameMain gm)
    {
        game = gm;
        Preferences pref = Gdx.app.getPreferences("Data");
        score = pref.getInteger("Score");

       // table.setSize(Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()/2);
        //table.setPosition(0, 0);

//        gameViewport = new FitViewport(GameInfo.WIDTH, GameInfo.HEIGHT,
//                new OrthographicCamera());
//        stage = new Stage(gameViewport, game.getBatch());
        stage = new Stage(new ScreenViewport());
        createAndPositionButtons();
        createLabel();
        stage.addActor(scoreLabel);
        stage.addActor(red);
        stage.addActor(blue);
        stage.addActor(yellow);
        stage.addActor(grey);
        stage.addActor(blueHat);
        stage.addActor(grumppy);
        Gdx.input.setInputProcessor(stage);
    }
    void createLabel() {

        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(
                Gdx.files.internal("04b_19.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter =
                new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 100;
        BitmapFont font = generator.generateFont(parameter);
        scoreLabel = new Label(String.valueOf("HIGH SCORE "+score),
                new Label.LabelStyle(font, Color.WHITE));
        scoreLabel.setPosition(GameInfo.WIDTH / 2f - scoreLabel.getWidth() / 2f,
                GameInfo.HEIGHT / 2f + 200);

    }

    void createAndPositionButtons() {
        red = new ImageButton(new SpriteDrawable(new Sprite(
                new Texture("red/red.png"))));
        red.setBounds(450,350,200,200);
        blue = new ImageButton(new SpriteDrawable(new Sprite(
                new Texture("blue/blue.png"))));
        blue.setBounds(650,350,200,200);
        yellow = new ImageButton(new SpriteDrawable(new Sprite(
                new Texture("yellow/yellow.png"))));
        yellow.setBounds(450 , 200 , 200 , 200 );
        grey = new ImageButton(new SpriteDrawable(new Sprite(
                new Texture("grey/grey.png"))));
        grey.setBounds(650 , 200 , 200 , 200);
        grumppy= new ImageButton(new SpriteDrawable(new Sprite(
                new Texture("grumpy/grumpy.png"))));
        grumppy.setBounds(450 , 50 , 200 , 200);
        blueHat = new ImageButton(new SpriteDrawable(new Sprite(
                new Texture("bluehat/bluehat.png"))));
        blueHat.setBounds(650 , 50 , 200 ,200);


        red.addListener(new ClickListener(){
            @Override
            public boolean touchDown(InputEvent event , float x , float y, int pointer, int button){
                player = "red";
                GameMain.screen = new MyGdxGame(game, player);
                shiftButtons();
                return true;
            }
        });
        blue.addListener(new ClickListener(){
            @Override
            public boolean touchDown(InputEvent event , float x , float y, int pointer, int button){
                player = "blue";
                GameMain.screen = new MyGdxGame(game, player);
                shiftButtons();
                return true;
            }
        });
        yellow.addListener(new ClickListener(){
            @Override
            public boolean touchDown(InputEvent event , float x , float y, int pointer, int button){
                player = "yellow";
                GameMain.screen = new MyGdxGame(game, player);
                shiftButtons();
                return true;
            }
        });
        grey.addListener(new ClickListener(){
            @Override
            public boolean touchDown(InputEvent event , float x , float y, int pointer, int button){
                player = "grey";
                GameMain.screen = new MyGdxGame(game, player);
                shiftButtons();
                return true;
            }
        });
        grumppy.addListener(new ClickListener(){
            @Override
            public boolean touchDown(InputEvent event , float x , float y, int pointer, int button){
                player = "grumpy";
                GameMain.screen = new MyGdxGame(game, player);
                shiftButtons();
                return true;
            }
        });
        blueHat.addListener(new ClickListener(){
            @Override
            public boolean touchDown(InputEvent event , float x , float y, int pointer, int button){
                player = "bluehat";
                GameMain.screen = new MyGdxGame(game, player);
                shiftButtons();
                return true;
            }
        });
    }

    public void shiftButtons(){

        grumppy.setBounds(-30 , 100 , 100 ,200);
        grey.setBounds(-30 , 100 , 100 ,200);
        yellow.setBounds(-30 , 100 , 100 ,200);
        red.setBounds(-30 , 100 , 100 ,200);
        blue.setBounds(-30 , 100 , 100 ,200);
        blueHat.setBounds(-30 , 100 , 100 ,200);

    }
    public Stage getStage(){
        return stage;
    }

//    public void render()
//    {
////        batch.begin();
//
//       // batch.draw(icebg , 0 , 0 , Gdx.graphics.getWidth() , Gdx.graphics.getHeight());
//        stage.act(Gdx.graphics.getDeltaTime()); //Perform ui logic
//        stage.draw(); //Draw the ui
////        if(player != null){
////            super.render();
////            setScreen(new MyGdxGame(batch , player));
////        }
//
////        batch.end();
//
//    }


}



