package mini.project;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

public class ReStartMessage {

        private Stage stage;
        Label scoreLabel ,  reStartMessage;
        int score ;

        public ReStartMessage()
        {
            Preferences pref = Gdx.app.getPreferences("Data");
            score = pref.getInteger("Score");
            stage = new Stage(new ScreenViewport());
            createReStartMessage();
            createLabel();
            stage.addActor(scoreLabel);
            stage.addActor(reStartMessage);
        }

        void createLabel() {
            FreeTypeFontGenerator generator = new FreeTypeFontGenerator(
                    Gdx.files.internal("04b_19.ttf"));
            FreeTypeFontGenerator.FreeTypeFontParameter parameter =
                    new FreeTypeFontGenerator.FreeTypeFontParameter();
            parameter.size = 100;
            BitmapFont font = generator.generateFont(parameter);
            scoreLabel = new Label(String.valueOf("HIGH SCORE "+score),
                    new Label.LabelStyle(font, Color.BLUE));
            scoreLabel.setPosition(GameInfo.WIDTH / 2f - scoreLabel.getWidth() / 2f,
                    GameInfo.HEIGHT / 2f + 200);
        }


        void createReStartMessage() {
            FreeTypeFontGenerator generator = new FreeTypeFontGenerator(
                    Gdx.files.internal("04b_19.ttf"));
            FreeTypeFontGenerator.FreeTypeFontParameter parameter =
                    new FreeTypeFontGenerator.FreeTypeFontParameter();
            parameter.size = 75;
            BitmapFont font = generator.generateFont(parameter);
            reStartMessage = new Label(String.valueOf("You Loss!! Tap to Re-start game"),
                    new Label.LabelStyle(font, Color.BLUE));
            reStartMessage.setPosition(20, Gdx.graphics.getHeight()/2);
        }
        public Stage getStage(){
            return stage;
        }

}
