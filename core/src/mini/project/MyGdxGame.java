package mini.project;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.PolygonRegion;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;


import java.util.Random;

import com.badlogic.gdx.graphics.g2d.PolygonRegion;
import com.badlogic.gdx.graphics.g2d.PolygonRegionLoader;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
public class MyGdxGame implements Screen {

	Sound winSound , damageSound , jumpSound;
	Texture img , bird;
	TextureAtlas birdAtlas;
	TextureAtlas beesAtlas;
	BitmapFont scoreF , startM;
	Animation<TextureRegion> animate;
	Animation<TextureRegion> beeFlap1;
	Animation<TextureRegion> beeFlap2;
	Animation<TextureRegion> beeFlap3;

	int life =0;
	float sw , sh , bw , bh , bx , by;
	float velocity = 0.0f , gravity = 0.1f;
	int state = 0;
	int bees = 3;
	boolean damageB;
	float[] beesx = new float[bees];
	ShapeRenderer sr ;
	Circle c_bird , c_bee1[] , c_bee2[] , c_bee3[];
	float[][] beesy = new float[bees][bees];
	Random r1;
	int score=0;
	float elapsedTime = 0.0f;
	boolean winB;
	String player = null;
	GameMain game;
	String[] background = {"ice1" , "ice2","ice3","ice4" ,"ice5"};
	Stage Font;
	Label scoreLabel , startMessage , reStart;
	UI ui;
	StartMesseg sm;
	ReStartMessage rms;
	PlayerScore ps;
	public MyGdxGame(GameMain game, String player){
		ui = new UI();
		ps = new PlayerScore();
		sm = new StartMesseg();
		rms = new ReStartMessage();
	    this.game = game;
		this.player = player;
		Random ir = new Random();
		img = new Texture("baground/"+background[ir.nextInt(5)]+".png");
		sr = new ShapeRenderer();

		damageB = false;

//		jumpSound = Gdx.audio.newSound(Gdx.files.internal("jump.ogg"));
//		damageSound = Gdx.audio.newSound(Gdx.files.internal("damage.ogg"));
		winSound = Gdx.audio.newSound(Gdx.files.internal("win.ogg"));

		scoreF = new BitmapFont();
		scoreF.setColor(Color.ROYAL);
		scoreF.getData().setScale(2);


		startM = new BitmapFont();
		startM.setColor(Color.BLUE);
		startM.getData().setScale(2);

		sw = Gdx.graphics.getWidth();
		sh =  Gdx.graphics.getHeight();
		bw =  Gdx.graphics.getWidth()/13;
		bh =  Gdx.graphics.getHeight()/11;
		bx =  Gdx.graphics.getHeight() / 4 ;
		by =  Gdx.graphics.getHeight() / 2;

//	    bee1 = new Texture("1.png");
//	    bee2 = new Texture("1.png");
//	    bee3 = new Texture("1.png");

		birdAtlas =  new TextureAtlas(player+"/"+player+""+".atlas");
		animate = new Animation<TextureRegion>(1f/7f , birdAtlas.getRegions() );

		beesAtlas = new TextureAtlas("bees/bees.atlas");
		beeFlap1 = new Animation<TextureRegion>(1/7f , beesAtlas.getRegions());

		beesAtlas = new TextureAtlas("bees/bees.atlas");
		beeFlap2 = new Animation<TextureRegion>(1/7f , beesAtlas.getRegions());

		beesAtlas = new TextureAtlas("bees/bees.atlas");
		beeFlap3 = new Animation<TextureRegion>(1/7f , beesAtlas.getRegions());



		c_bird = new Circle();
		c_bee1 = new Circle[bees];
		c_bee2 = new Circle[bees];
		c_bee3 = new Circle[bees];
		for(int i = 0 ; i < bees ; i++){
			beesx[i] = sw + i * (sw / 2) ;
			r1 = new Random();
			beesy[0][i] =  r1.nextInt((520)+1 )+200 +r1.nextFloat();
			if(score>100)beesy[1][i] =  r1.nextInt((150)+1 )+100 +r1.nextFloat();
			if(score>200)beesy[2][i] =  r1.nextInt((100)+1 )+200 +r1.nextFloat();

			c_bee1[i] = new Circle();
			c_bee2[i] = new Circle();
			c_bee3[i] = new Circle();
		}
	}

	public void setStartM(){
			FreeTypeFontGenerator generator = new FreeTypeFontGenerator(
					Gdx.files.internal("04b_19.ttf"));
			FreeTypeFontGenerator.FreeTypeFontParameter parameter =
					new FreeTypeFontGenerator.FreeTypeFontParameter();
			parameter.size = 100;
			BitmapFont font = generator.generateFont(parameter);
			scoreLabel = new Label(String.valueOf("HIGH SCORE "+score),
					new Label.LabelStyle(font, Color.WHITE));
			startMessage = new Label(String.valueOf("Tap to Start Game"),
					new Label.LabelStyle(font, Color.WHITE));
			scoreLabel.setPosition(sw/3 + 30 , sh /2 + 50);
			startMessage.setPosition(sw/3 , sh/2);
	}
	public void setReStart(){
			FreeTypeFontGenerator generator = new FreeTypeFontGenerator(
					Gdx.files.internal("04b_19.ttf"));
			FreeTypeFontGenerator.FreeTypeFontParameter parameter =
					new FreeTypeFontGenerator.FreeTypeFontParameter();
			parameter.size = 100;
			BitmapFont font = generator.generateFont(parameter);
			scoreLabel = new Label(String.valueOf("HIGH SCORE "+score),
					new Label.LabelStyle(font, Color.WHITE));
			reStart = new Label("You loss!! Tap to Re-Start Game" ,
					new Label.LabelStyle(font,Color.WHITE));
			scoreLabel.setPosition(sw/3 + 30 , sh /2 + 50);
			reStart.setPosition(sw/3 , sh/2);
	}
	@Override
	public void show () {


	}

	@Override
	public void render(float delta){
		game.batch.begin();
		game.batch.draw(img , 0 ,0  , sw ,sh);

//		batch.draw(bird , bx , by , bw , bh);

		elapsedTime += Gdx.graphics.getDeltaTime();
		TextureRegion currentBirdFrame = animate.getKeyFrame(elapsedTime , true);
		game.batch.draw( currentBirdFrame , bx , by , bw , bh);

		TextureRegion currentBeeFrame1 = beeFlap1.getKeyFrame(elapsedTime , true);
		TextureRegion currentBeeFrame2 = beeFlap2.getKeyFrame(elapsedTime , true);

		TextureRegion currentBeeFrame3  = beeFlap3.getKeyFrame(elapsedTime , true);

		if(state == 1) {

//			for(int i = 0 ; i < bees ; i++){
//				System.out.println(beesx[i] + " "+beesy[0][i] +" "+beesy[1][i]+" "+beesy[2][i]);
//			}
			if (Gdx.input.isTouched()) {
				velocity = -3;
			}
			for(int i = 0 ; i < bees ; i++){
				if(beesx[i] < 0){
					score++;
					ui.setScore(score);
					if (score %50 == 0) winB = true;
					beesx[i] = bees *  sw / 2 ;
					beesy[0][i] =  r1.nextInt(630) +r1.nextFloat();
					if(score>100)  beesy[1][i] =  r1.nextInt((100)+1 )+150 +r1.nextFloat();
					if(score>200)	beesy[2][i] =  r1.nextInt((100)+1 )+200 +r1.nextFloat();
				}
				beesx[i] -= 6;
				game.batch.draw(currentBeeFrame1 , beesx[i] , beesy[0][i] , bw , bh);
				if(score>100)	game.batch.draw(currentBeeFrame2 , beesx[i] , beesy[1][i] , bw , bh);
				if(score>200)	game.batch.draw(currentBeeFrame3 ,beesx[i] , beesy[2][i] , bw , bh);

			}
			//sr.begin(ShapeRenderer.ShapeType.Filled);
			if( score % 50 == 0 && winB) {
				winSound.play();
				winB = false;
			}
			startM.draw(game.batch , score+"" , sw - 100 , 50);
			ui.getStage().draw();
			//sr.end();
			if(by > 650){
				by = 650;

			}
			if (by < 0) {
				by = Gdx.graphics.getHeight() / 3;
				velocity = 0;
				state = 2;
			}
			velocity = velocity + gravity;
			by = by - velocity;
		}else if(state ==0){
			Preferences pref = Gdx.app.getPreferences("Data");
			startM.draw(game.batch, "High Score "+pref.getInteger("Score"), sw/3 + 30 , sh /2 + 50);
			startM.draw(game.batch,"Tap to Start Game	", sw/3 , sh/2);
			sm.getStage().draw();
			sm.getStage().act();
			if(Gdx.input.justTouched()){
				state = 1;
			}
		}else if(state == 2){

			startM.draw(game.batch,"You loss!! Tap to Re-Start Game" , sw/3 , sh/2);

			Preferences pref = Gdx.app.getPreferences("Data");
			int highSore =  pref.getInteger("Score");

//
			startM.draw(game.batch, "High Score is "+pref.getInteger("Score"), sw/3 + 30 , sh /2 + 50);
			rms.getStage().draw();
			rms.getStage().act();
			ps.setScore("Your Score "+score);
			ps.getStage().draw();
            ps.getStage().act();
			if(score > highSore){
				pref.putInteger("Score" , score);
				pref.flush();
			}
			if(Gdx.input.isTouched()){
				//score = 0;
				bx = Gdx.graphics.getWidth()/5;
				by = Gdx.graphics.getHeight()/2;
				for(int i = 0 ; i < bees ; i++){
					beesx[i] = sw + i * (sw / 2) ;
					r1 = new Random();
					beesy[0][i] =  r1.nextInt((200)+1 )+200 +r1.nextFloat();
					if(score>100)	beesy[1][i] =  r1.nextInt((150)+1 )+100 +r1.nextFloat();
					if(score>200)	beesy[2][i] =  r1.nextInt((100)+1 )+200 +r1.nextFloat();

					c_bee1[i] = new Circle();
					if(score>100)	c_bee2[i] = new Circle();
					if(score>200)	c_bee3[i] = new Circle();
				}
				state = 1;
				score = 0;
				ui.setScore(0);
			}
		}
       sr.begin(ShapeRenderer.ShapeType.Filled);

		c_bird.set((bx + bw/2) , (by + bh/2) +3, bw/3);
		for(int i = 0 ; i < bees; i++){
//				sr.setColor(Color.BLUE);
//				sr.circle((beesx[i] + bw/2 ), beesy[0][i] + bh /2 , bw/3);
////				sr.circle(beesx[i] + bw/2, beesy[1][i] + bh/2 , bw/3);
////				sr.circle(beesx[i] + bw/2 , beesy[2][i] + bh/2 , bw/3);
//				sr.circle((bx + bw/2) , (by + bh/2)+3 , bw/3);

			c_bee1[i].set((beesx[i] + bw/2) , beesy[0][i] + bh /2 , bw/3);
			if(score>100)c_bee2[i].set(beesx[i] + bw/2 , beesy[1][i] + bh /2 , bw/3);
			if(score>200)c_bee3[i].set(beesx[i] + bw/2 , beesy[2][i] + bh /2 , bw/3);

			if(score<100){
				if(Intersector.overlaps(c_bird , c_bee1[i])){
					state = 2;
					//game.setScreen(new MainMenu(game));
				}
			}
			if(score>100){
				if(Intersector.overlaps(c_bird , c_bee1[i]) || Intersector.overlaps(c_bird , c_bee2[i])){
					state = 2;
				}
			}

			if(score>200) {
				if (Intersector.overlaps(c_bird, c_bee1[i]) || Intersector.overlaps(c_bird, c_bee2[i]) || Intersector.overlaps(c_bird, c_bee3[i])) {
					state = 2;
				}
			}
		}
		sr.end();
		game.batch.end();
	}

	/**
	 * Called when this screen becomes the current screen for a {@link Game}.
	 */


	/**
	 * Called when the screen should render itself.
	 *
	 * @param delta The time in seconds since the last render.
	 */

	/**
	 * @param width
	 * @param height
	 * @see ApplicationListener#resize(int, int)
	 */
	@Override
	public void resize(int width, int height) {

	}

	/**
	 * @see ApplicationListener#pause()
	 */
	@Override
	public void pause() {

	}

	/**
	 * @see ApplicationListener#resume()
	 */
	@Override
	public void resume() {

	}

	/**
	 * Called when this screen is no longer the current screen for a {@link Game}.
	 */
	@Override
	public void hide() {

	}

	@Override
	public void dispose () {
		game.batch.dispose();
		img.dispose();
	}
	void createLabel() {

		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(
				Gdx.files.internal("04b_19.ttf"));
		FreeTypeFontGenerator.FreeTypeFontParameter parameter =
				new FreeTypeFontGenerator.FreeTypeFontParameter();
		parameter.size = 100;
		BitmapFont font = generator.generateFont(parameter);
		scoreLabel = new Label(String.valueOf("HIGH SCORE "+score),
				new Label.LabelStyle(font, Color.WHITE));
		scoreLabel.setPosition(GameInfo.WIDTH / 2f - scoreLabel.getWidth() / 2f,
				GameInfo.HEIGHT / 2f + 200);

	}
}
