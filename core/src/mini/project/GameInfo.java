package mini.project;


import com.badlogic.gdx.Gdx;

/**
 * Created by Fahir on 5/4/16.
 */
public class GameInfo {

    public static final int WIDTH = Gdx.graphics.getWidth();
    public static final int HEIGHT = Gdx.graphics.getHeight();
    public static final float PPM = 100f;



}
